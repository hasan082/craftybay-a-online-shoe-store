# CraftyBay - Your Ultimate Crafting Supplies Shop

## Overview

CraftyBay is a feature-rich e-commerce application designed to meet all your crafting needs. Whether you're an experienced artist or just starting, CraftyBay offers a wide range of crafting supplies, tools, and inspiration. Shop for the finest materials, explore crafting ideas, and elevate your creative projects.

### Feature-Based MVVM Architecture

CraftyBay is built upon a robust and maintainable architecture known as Feature-Based MVVM (Model-View-ViewModel). This architectural pattern helps organize the codebase efficiently and promotes a separation of concerns. Here's how it's applied within the app:

- **Model**: Represents the data and business logic. In CraftyBay, this includes data models, API handling, and state management using packages like `get`.

- **View**: Represents the user interface and is responsible for rendering the user interface components. CraftyBay's UI components are organized into feature-specific screens and widgets.

- **ViewModel**: Acts as an intermediary between the Model and View layers. It manages the app's state, business logic, and data flow. CraftyBay utilizes ViewModels to ensure that the UI remains responsive and data-driven.

This architectural choice enables CraftyBay to maintain a clean and modular codebase, making it easier to add new features, scale the application, and enhance maintainability.


## Screenshot

![Screenshot](craftybay.jpg)

## Project Structure

CraftyBay follows a well-organized project structure for maintainability and scalability:

- **assets**: Contains static assets like images and fonts used in the app.
- **lib**: The heart of the application.
    - **application**: Core logic and management.
        - `app.dart`: Entry point for the CraftyBay app.
        - `app_binding.dart`: Dependency injection setup using GetX library.
        - `theme_manager.dart`: Custom theme management.
    - **data**: Data layer for API handling and data models.
        - `api_handler`: Utility for handling API requests.
        - `api_models`: Data models representing API responses.
    - **presentation**: Presentation layer with UI components and controllers.
        - **state_holders**: Controllers for managing app states.
            - `email_verify_controller.dart`: Controller for email verification.
            - `otp_controller.dart`: Controller for OTP verification.
            - `profile_update_controller.dart`: Controller for user profile updates.
            - `splash_controller.dart`: Controller for the app's splash screen.
        - **ui**: User interface components.
            - **screen**: Individual app screens.
                - `auth_and_profile_screen`: Screens for authentication and user profiles.
                - `bottom_nav_screen.dart`: Main screen with a bottom navigation bar.
                - `home_screen.dart`: Home screen displaying products and categories.
                - `notification_screen.dart`: Screen for user notifications.
                - `product_details_screen.dart`: Screen displaying product details.
                - `splash_screen.dart`: Splash screen shown on app startup.
            - **utility**: Utility widgets and classes.
                - `image_utils.dart`: Utility for image handling.
                - `color_palate.dart`: Definition of the color palette.
            - **widgets**: Reusable UI widgets.
                - **Home**: Widgets specific to the home screen.
                  - `carousel_header_widget.dart`: Widget for the carousel header.
                  - `home_slider_card_widget.dart`: Widget for slider cards on the home screen.
                - **product**: Widgets related to product display.
                  - `category_card.dart`: Widget for displaying product categories.
                  - `product_card.dart`: Widget for displaying individual product cards.
                - `app_bar_widget.dart`: Custom app bar widget.
                - `custom_bottom_navigation_bar_widget.dart`: Custom bottom navigation bar widget.
                - `custom_search_widget.dart`: Custom search widget.
                - `header_description_widget.dart`: Header and description widget.
                - `resuable_text_field.dart`: Reusable text field widget.
    - `main.dart`: App entry point.

## Features

- **User Authentication**: Register and log in securely.
- **Product Browsing**: Explore a vast collection of crafting supplies.
- **Product Details**: View detailed product information.
- **Shopping Cart**: Add and manage items in your shopping cart.
- **Wishlist**: Save favorite products for later.
- **User Profile**: Update personal information with ease.
- **Responsive Design**: CraftyBay is designed for mobile and web platforms.

## Getting Started

1. Clone the repository: `git clone https://github.com/yourusername/CraftyBay.git`
2. Navigate to the project directory: `cd CraftyBay`
3. Run the app: `flutter run`

## Dependencies

CraftyBay leverages several dependencies for enhanced functionality:

- **flutter_svg (^2.0.7)**: This package is used for rendering SVG (Scalable Vector Graphics) images
  within the app. It allows for flexible and high-quality vector graphics display.

- **get (^4.6.6)**: Get is a powerful state management library for Flutter. It simplifies state
  management and provides a robust mechanism for dependency injection, making it ideal for managing
  app-wide state and navigation.

- **package_info_plus (^4.1.0)**: This package provides access to information about the application
  package, such as its name, version, and build number. It's used to display app information within
  the user interface.

- **pin_code_fields (^8.0.1)**: Pin Code Fields is employed for creating customizable PIN code input
  fields within CraftyBay. It facilitates the implementation of OTP (One-Time Password) verification
  screens and similar functionality.

- **http (^1.1.0)**: The HTTP package is used for making HTTP requests to APIs and web services. It
  enables CraftyBay to communicate with external data sources and retrieve information.

- **animated_bottom_navigation_bar (^1.2.0)**: This package enhances the app's user interface by
  providing animated bottom navigation bars. It adds interactive and visually appealing navigation
  elements.

- **carousel_slider (^4.2.1)**: Carousel Slider is used to create image carousels and sliders within
  CraftyBay. It allows for the display of multiple images or items in a horizontally scrolling
  format.

## License

This project is licensed under the MIT License. See
the [LICENSE](https://opensource.org/license/mit/) file for details.

