import 'package:craftyBay/application/theme_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../presentation/ui/screen/splash_screen.dart';
import 'app_binding.dart';

class CraftyBay extends StatelessWidget {
  const CraftyBay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: AppBindings(),
      debugShowCheckedModeBanner: false,
      theme: ThemeManager.getAppTheme(),
      home: const SplashScreen(),
    );
  }
}
