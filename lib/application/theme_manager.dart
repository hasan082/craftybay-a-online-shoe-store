import 'package:flutter/material.dart';
import '../presentation/ui/utility/color_palate.dart';

class ThemeManager {
  static ThemeData getAppTheme() {
    return ThemeData(
      primaryColor: ColorPalate.primeColor,
      primarySwatch: MaterialColor(ColorPalate.primeColor.value, ColorPalate.color),
      inputDecorationTheme: InputDecorationTheme(
        hintStyle: TextStyle(color: Colors.grey.shade400, fontSize: 17),
        contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 0),
        border: const OutlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalate.primeColor,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalate.primeColor.withOpacity(.75),
          ),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalate.primeColor,
          ),
        ),
        disabledBorder: const OutlineInputBorder(),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white,
          padding: const EdgeInsets.symmetric(vertical: 14),
          textStyle: const TextStyle(fontSize: 17),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      ),
      appBarTheme: const AppBarTheme(
        color: Colors.white,
        elevation: 0,
        titleTextStyle: TextStyle(color: Colors.white, fontSize: 22)
      ),
    );
  }
}
