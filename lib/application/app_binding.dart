import 'package:get/get.dart';
import '../presentation/state_holders/email_verify_controller.dart';
import '../presentation/state_holders/otp_controller.dart';
import '../presentation/state_holders/profile_update_controller.dart';
import '../presentation/state_holders/splash_controller.dart';

class AppBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<SplashController>(SplashController());
    Get.put<EmailVerifyController>(EmailVerifyController());
    Get.put<OtpVerifyController>(OtpVerifyController());
    Get.put<ProfileUpdateController>(ProfileUpdateController());
  }
}
