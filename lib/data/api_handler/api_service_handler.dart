import 'dart:convert';
import 'dart:async';
import 'dart:developer';
import 'package:craftyBay/data/api_models/api_response.dart';
import 'package:http/http.dart';

class NetworkServiceHandler {

  Future<ApiResponse> getRequest(String url) async {
    try{
      Response response = await get(Uri.parse(url), headers: {
        'Content-type': 'application/json',
        // 'token': AuthUtility.userInfo.token.toString()
      });
      if(response.statusCode == 200){
        return ApiResponse(true, response.statusCode, jsonDecode(response.body));
      }else {
        return ApiResponse(false, response.statusCode, null);
      }
    }catch(error){
      log(error.toString());
    }
    return ApiResponse(false, -1, null);
  }

  Future<ApiResponse> postRequest(String url, Map<String, dynamic> body,{bool isLogin = false}) async {
    try{
      Response response = await post(Uri.parse(url),headers: {
        'Content-Type': 'application/json',
        // 'token': AuthUtility.userInfo.token.toString()
      }, body: jsonEncode(body));
      if(response.statusCode == 200){
        return ApiResponse(true, response.statusCode, jsonDecode(response.body));
      }else if (response.statusCode == 401){
        if(isLogin==false){
          // backToLogin();
        }
      }else {
        return ApiResponse(false, response.statusCode, null);
      }
    }catch(error){
      log(error.toString());
    }
    return ApiResponse(false, -1, null);
  }

  //ToDo==== I have fixed below function


  // Future<void> backToLogin() async {
  //   await AuthUtility.clearUserInfo();
  //   Navigator.pushNamedAndRemoveUntil(UTask.globalKey.currentContext!, '/login', (route) => false);
  // }




}