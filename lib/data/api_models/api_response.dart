class ApiResponse {
  final int statusCode;
  final bool isSuccess;
  final Map<String, dynamic>? body;

  ApiResponse(this.isSuccess, this.statusCode, this.body);
}