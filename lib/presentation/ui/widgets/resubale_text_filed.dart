import 'package:flutter/material.dart';

class ReusableTextFormFiled extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final int? minLines;
  final String? labelText;
  final bool? obscureText;
  final TextInputType? keyboardType;
  final String Function(String?)? validator;

  const ReusableTextFormFiled({
    super.key,
    required this.controller,
    required this.hintText,
    required this.validator,
    this.minLines,
    this.labelText,
    this.keyboardType,
    this.obscureText,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        hintText: hintText,
        labelText: labelText,
        border: const OutlineInputBorder(),
          contentPadding: const EdgeInsets.symmetric(
            vertical: 7,
            horizontal: 12
          )
      ),
      minLines: minLines,
      maxLines: minLines,
      validator: validator,
    );
  }
}
