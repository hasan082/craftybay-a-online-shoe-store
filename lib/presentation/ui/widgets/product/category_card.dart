import 'package:craftyBay/presentation/ui/screen/bottom_nav_screen/wishlist_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class CategoryCard extends StatelessWidget {
  final String cateIcon;
  const CategoryCard({
    super.key,
    required this.cateIcon,
  });



  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Get.to(()=>WishListScreen());
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: const Color(0xffE7F7F7),
        ),
        width: 80,
        height: 80,
        child: Image.asset(cateIcon, width: double.maxFinite,),
      ),
    );
  }
}