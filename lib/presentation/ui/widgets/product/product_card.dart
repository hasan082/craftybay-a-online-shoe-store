import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../screen/product_details_screen.dart';
import '../../utility/color_palate.dart';

class ProductCard extends StatelessWidget {
  final String productImgUrl;
  final String cardProductTitle;
  final double price;
  final double? rating;

  const ProductCard({
    super.key,
    required this.productImgUrl,
    required this.cardProductTitle,
    required this.price,
    this.rating,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(7),
      onTap: () {
        Get.to(() => ProductDetailsScreen());
      },
      child: Card(
        elevation: 2,
        shadowColor: ColorPalate.primeColor.withOpacity(.7),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: const BorderSide(
            color: Colors.transparent,
            width: 0,
          ),
        ),
        child: SizedBox(
          width: 130,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 80,
                width: double.maxFinite,
                decoration: BoxDecoration(
                  color: ColorPalate.primeColor.withOpacity(.3),
                  borderRadius: BorderRadius.circular(7),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Image.asset(
                    productImgUrl,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(6),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      cardProductTitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: Colors.blueGrey,
                      ),
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '\$$price',
                          style: const TextStyle(
                              color: ColorPalate.primeColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w700),
                        ),
                        Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            const Icon(
                              Icons.star,
                              color: Colors.amber,
                              size: 14,
                            ),
                            Text(
                              rating != null ? rating.toString() : 'n/a',
                              textScaleFactor: 1.0,
                              style: const TextStyle(
                                fontSize: 12,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: ColorPalate.primeColor,
                                  borderRadius: BorderRadius.circular(3)),
                              child: const Padding(
                                padding: EdgeInsets.all(2),
                                child: Icon(
                                  Icons.favorite_outline,
                                  size: 14,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
//ToDo
//https://ostad.app/dashboard/my-courses/63d551dca64614e5b7bd4392/recordings?play=65013ca22c0d2c1e2ab8b924