import 'package:flutter/material.dart';

class AboveHeaderWidget extends StatelessWidget {
  final String catTitle;
  final VoidCallback onTap;
  const AboveHeaderWidget({
    super.key, required this.catTitle, required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          catTitle,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w600,
            letterSpacing: 0.5,
          ),
        ),
        const Spacer(),
        TextButton(
          onPressed: onTap,
          child: const Text('See All', style: TextStyle(fontSize: 18),),
        ),
      ],
    );
  }
}