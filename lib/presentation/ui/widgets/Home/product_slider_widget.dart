import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import '../../utility/color_palate.dart';

class ProductCarouselWithDots extends StatefulWidget {
  final List<String> items;

  const ProductCarouselWithDots({super.key, required this.items});
  
  @override
  State<ProductCarouselWithDots> createState() =>
      _ProductCarouselWithDotsState();
}

class _ProductCarouselWithDotsState extends State<ProductCarouselWithDots> {
  int current = 0;
  final CarouselController _sliderController = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CarouselSlider.builder(
          carouselController: _sliderController,
          itemCount: widget.items.length,
          itemBuilder: (context, index, realIndex) {
            return CarouselSliderBlock(
              widget: widget,
              index: index,
            );
          },
          options: CarouselOptions(
            height: 300,
            autoPlay: true,
            enlargeCenterPage: true,
            viewportFraction: 1.0,
            autoPlayInterval: const Duration(seconds: 3),
            autoPlayAnimationDuration: const Duration(milliseconds: 1000),
            autoPlayCurve: Curves.fastOutSlowIn,
            enlargeFactor: 0.15,
            onPageChanged: (index, reason) {
              setState(() {
                current = index;
              });
            },
          ),
        ),
        Positioned(
          bottom: 15,
          left: 0,
          right: 0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: widget.items.asMap().entries.map((entry) {
              int index = entry.key;
              return GestureDetector(
                onTap: () {
                  _sliderController.animateToPage(index);
                },
                child: DotsWidget(current: current, index: index),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }
}

class CarouselSliderBlock extends StatelessWidget {
  const CarouselSliderBlock({
    super.key,
    required this.widget,
    required this.index,
  });

  final int index;
  final ProductCarouselWithDots widget;

  @override
  Widget build(BuildContext context) {

    return Expanded(
      child: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Color(0xFFF3F3F3),
        ),
        child: FittedBox(
          child: SizedBox(
            height: 140,
            child: Padding(
              padding:
                  const EdgeInsets.only(top: 56.0, left: 20, right: 20, bottom: 20),
              child: Image.asset(
                widget.items[index], // Set the desired width
              ),
            ),
          ),
        ),
      ),
    );
  }
}


class DotsWidget extends StatelessWidget {
  const DotsWidget({
    super.key,
    required this.current,
    required this.index,
  });

  final int current;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 12,
      height: 12,
      margin: const EdgeInsets.symmetric(horizontal: 4),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: current == index ? ColorPalate.primeColor : Colors.grey,
        ),
        shape: BoxShape.circle,
        color: current == index ? ColorPalate.primeColor : Colors.transparent,
      ),
    );
  }
}
