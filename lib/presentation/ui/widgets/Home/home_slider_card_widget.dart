
import 'package:flutter/material.dart';

import '../../utility/color_palate.dart';

class HomeSliderCard extends StatelessWidget {
  final String imgUrl;
  final String productTitle;
  final VoidCallback onTap;
  const HomeSliderCard({
    super.key,
    required this.imgUrl, required this.productTitle, required this.onTap,
  });


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 250,
      decoration: BoxDecoration(
        color: ColorPalate.primeColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width:
              MediaQuery.of(context).size.width * 0.45 - 30,
              child: Image.asset(
                imgUrl,
                width: double.maxFinite,
                height: double.maxFinite,
                fit: BoxFit.contain,
              ),
            ),
            const SizedBox(width: 12),
            SizedBox(
              width:
              MediaQuery.of(context).size.width * 0.51 - 30,
              child: Expanded(
                child: Column(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      productTitle,
                      maxLines: 3,
                      softWrap: true,
                      style: const TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                        backgroundColor: Colors.white,
                      ),
                      onPressed: onTap,
                      child: const Text(
                        'Buy Now',
                        style: TextStyle(
                            color: ColorPalate.primeColor),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
