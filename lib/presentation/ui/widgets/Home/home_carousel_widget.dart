import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import '../../utility/color_palate.dart';






class CarouselWithDots extends StatefulWidget {
  final List<Widget> items;

  const CarouselWithDots({super.key, required this.items});

  @override
  State<CarouselWithDots> createState() => _CarouselWithDotsState();
}

class _CarouselWithDotsState extends State<CarouselWithDots> {
  int current = 0;
  final CarouselController _sliderController = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider.builder(
          carouselController: _sliderController,
          itemCount: widget.items.length,
          itemBuilder: (context, index, realIndex) {
            return widget.items[index];
          },
          options: CarouselOptions(
            autoPlay: true,
            enlargeCenterPage: true,
            viewportFraction: 1.0,
            autoPlayInterval: const Duration(seconds: 3),
            autoPlayAnimationDuration: const Duration(milliseconds: 1000),
            autoPlayCurve: Curves.fastOutSlowIn,
            enlargeFactor: 0.15,
            onPageChanged: (index, reason) {
              setState(() {
                current = index;
              });
            },
          ),
        ),
        const SizedBox(height: 15,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widget.items.asMap().entries.map((entry) {
            int index = entry.key;
            return GestureDetector(
              onTap: (){
                _sliderController.animateToPage(index);
              },
              child: Container(
                width: 12,
                height: 12,
                margin: const EdgeInsets.symmetric(horizontal: 4),
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: current == index
                        ? ColorPalate.primeColor // Active dot color
                        : Colors.grey, // Active dot color
                  ),
                  shape: BoxShape.circle,
                  color: current == index
                      ? ColorPalate.primeColor // Active dot color
                      : Colors.transparent, // Inactive dot color
                ),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }
}
