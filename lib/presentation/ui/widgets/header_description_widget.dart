import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../utility/image_utils.dart';

class HeaderDescriptionWidget extends StatelessWidget {
  final String title;
  final String description;
  const HeaderDescriptionWidget({Key? key, required this.title, required this.description}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Column(
      children: [
        SvgPicture.asset(
          ImageUtils.logoSVG,
          width: 100,
        ),
        const SizedBox(height: 10),
        Text(
          title,
          style: textTheme.titleLarge?.copyWith(
            fontSize: 22,
          ),
        ),
        const SizedBox(height: 12),
        Text(
          description,
          style: textTheme.titleLarge?.copyWith(
            fontSize: 16,
            color: Colors.grey,
          ),
        ),
      ],
    );
  }
}
