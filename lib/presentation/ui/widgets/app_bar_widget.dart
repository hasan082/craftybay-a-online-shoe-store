import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../screen/auth/email_verification_screen.dart';
import '../screen/notification_screen.dart';
import '../utility/image_utils.dart';


class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {

  const CustomAppBar({super.key,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Row(
        children: [
          SvgPicture.asset(ImageUtils.navLogoSVG),
          const Spacer(),
          Row(
            children: [
              AppbarIconButton(
                icon: Icons.person_outline,
                onTap: (){
                  Get.to(()=> EmailVerificationScreen(),);
                },
              ),
              const SizedBox(width: 8,),
              AppbarIconButton(
                icon: Icons.phone_outlined,
                onTap: (){
                  Get.to(()=> EmailVerificationScreen(),);
                },
              ),
              const SizedBox(width: 8,),
              AppbarIconButton(
                icon: Icons.notifications_outlined,
                onTap: (){
                  Get.to(()=> const NotificationScreen(),);
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class AppbarIconButton extends StatelessWidget {
  final IconData icon;
  final VoidCallback? onTap;
  const AppbarIconButton({
    super.key, required this.icon, this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: CircleAvatar(
        backgroundColor: Colors.grey.shade200,
        radius: 20,
        child: Icon(icon, color: Colors.grey,size: 20,),
      ),
    );
  }
}
