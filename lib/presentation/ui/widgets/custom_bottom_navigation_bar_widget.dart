import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:craftyBay/presentation/ui/utility/color_palate.dart';
import 'package:flutter/material.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  final int selectedIndex;
  final PageController pageController;
  final void Function(int) onTap;
  final List<PageData> pageData;

  const CustomBottomNavigationBar({
    Key? key,
    required this.selectedIndex,
    required this.pageController,
    required this.onTap,
    required this.pageData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBottomNavigationBar.builder(
      itemCount: pageData.length,
      tabBuilder: (int index, bool isActive) {
        final color = isActive ? ColorPalate.primeColor : Colors.grey;
        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 2),
            Icon(
              pageData[index].icon,
              size: 18,
              color: color,
            ),
            const SizedBox(height: 1),
            Text(
              pageData[index].title,
              style: TextStyle(fontSize: 14, color: color),
            ),
            const SizedBox(height: 3),
          ],
        );
      },
      backgroundColor: Colors.white.withOpacity(.9),
      activeIndex: selectedIndex,
      splashColor: ColorPalate.primeColor,
      splashSpeedInMilliseconds: 300,
      notchSmoothness: NotchSmoothness.defaultEdge,
      gapLocation: GapLocation.none,
      leftCornerRadius: 25,
      rightCornerRadius: 25,
      onTap: onTap,
      shadow: const BoxShadow(
        offset: Offset(0, 3),
        blurRadius: 3,
        spreadRadius: 0.8,
      ),
    );
  }
}

class PageData {
  final Widget page;
  final IconData icon;
  final String title;

  PageData({
    required this.page,
    required this.icon,
    required this.title,
  });
}
