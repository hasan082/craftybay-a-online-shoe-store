import 'package:craftyBay/presentation/state_holders/splash_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../utility/image_utils.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  Widget _buildLogoWidget() {
    return SvgPicture.asset(
      ImageUtils.logoSVG,
      width: 100,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const Spacer(),
              _buildLogoWidget(),
              const Spacer(),
              const CircularProgressIndicator(),
              const SizedBox(height: 10),
              GetBuilder<SplashController>(
                builder: (splashController) {
                  return Text(splashController.versionNumber);
                },
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
