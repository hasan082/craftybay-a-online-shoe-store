import 'package:flutter/material.dart';

import '../widgets/Home/product_slider_widget.dart';

class ProductDetailsScreen extends StatelessWidget {
  ProductDetailsScreen({Key? key}) : super(key: key);

  final List<String> carouselImg = [
    'assets/images/product_img/main_img.png',
    'assets/images/product_img/slider_1.png',
    'assets/images/product_img/slider_2.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          ProductCarouselWithDots(
            items: carouselImg,
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: AppBar(
              backgroundColor: Colors.transparent,
              title: const Text('Details View', style: TextStyle(color: Colors.black54),),
            ),
          ),
        ],
      ),
    ));
  }
}
