import 'package:craftyBay/presentation/ui/utility/color_palate.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import '../../../state_holders/otp_controller.dart';
import '../../widgets/header_description_widget.dart';

class OtpVerifyScreen extends StatefulWidget {
  final String email;

  const OtpVerifyScreen({Key? key, required this.email}) : super(key: key);

  @override
  State<OtpVerifyScreen> createState() => _OtpVerifyScreenState();
}

class _OtpVerifyScreenState extends State<OtpVerifyScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final OtpVerifyController otpVerifyController = Get.find<OtpVerifyController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      otpVerifyController.startCountDownTimer();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const HeaderDescriptionWidget(
                      title: 'Enter OTP CODE',
                      description: 'A 4 Digit Code Has Been Sent',
                    ),
                    const SizedBox(height: 15),
                    _pinFieldWidget(context),
                    const SizedBox(height: 15),
                    _buildNextButton(),
                    const SizedBox(height: 15),
                    Obx(
                          () {
                        final otpIntController = Get.find<OtpVerifyController>();
                        return RichText(
                          text: TextSpan(
                            style: const TextStyle(color: Colors.grey, fontSize: 16),
                            children: <TextSpan>[
                              const TextSpan(
                                text: 'The code will expire after ',
                              ),
                              TextSpan(
                                text: '${otpIntController.countDown.value}s',
                                style: const TextStyle(
                                  color: ColorPalate.primeColor,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    Obx(
                          () {
                        final otpBoolController = Get.find<OtpVerifyController>();
                        return TextButton(
                          onPressed: () {
                            otpBoolController.resetCountdownTimer();
                          },
                          style: TextButton.styleFrom(
                            foregroundColor: otpBoolController.isFinished.value
                                ? ColorPalate.primeColor // Enabled state color
                                : Colors.grey, // Disabled state color
                          ),
                          child: const Text('Resend Code'),
                          );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _pinFieldWidget(BuildContext context) {
    return PinCodeTextField(
      controller: otpVerifyController.otpCtrl,
      length: 4,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(2),
        fieldHeight: 50,
        fieldWidth: 45,
        activeColor: ColorPalate.primeColor,
        inactiveColor: ColorPalate.primeColor,
        selectedColor: ColorPalate.primeColor,
        borderWidth: 2,
        selectedFillColor: Colors.white,
        inactiveFillColor: Colors.white,
        activeFillColor: Colors.white,
      ),
      cursorColor: ColorPalate.primeColor,
      autoDisposeControllers: false,
      backgroundColor: Colors.transparent,
      enableActiveFill: true,
      appContext: context,
      onCompleted: (pin) {},
      validator: (value){
        if(value == null || value.isEmpty) {
          return 'Please enter otp';
        } else if(value.length != 4) {
          return 'Please enter 4 digit otp';
        }
      },
    );
  }

  Widget _buildNextButton() {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {
          if (!_formKey.currentState!.validate()) {
            return;
          }
          otpVerifyController.gotoNextScreenWithOtpAndEmail(
            widget.email,
            otpVerifyController.otpCtrl.text,
          );
          otpVerifyController.otpCtrl.clear();
        },
        child: const Text('Next'),
      ),
    );
  }
}
