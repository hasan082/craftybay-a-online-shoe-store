import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../state_holders/email_verify_controller.dart';
import '../../widgets/header_description_widget.dart';

class EmailVerificationScreen extends StatelessWidget {
  EmailVerificationScreen({Key? key}) : super(key: key);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final EmailVerifyController emailVerifyController = Get.find<EmailVerifyController>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const HeaderDescriptionWidget(
                      title: 'Welcome back',
                      description: 'Please Enter Your Email Address',
                    ),
                    const SizedBox(height: 12),
                    _buildEmailInput(),
                    const SizedBox(height: 12),
                    _buildNextButton(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }


  Widget _buildEmailInput() {
    return GetBuilder<EmailVerifyController>(
      builder: (emailVerifyController) {
        return TextFormField(
          controller: emailVerifyController.emailCtrl,
          keyboardType: TextInputType.emailAddress,
          decoration: const InputDecoration(
            hintText: 'Email address',
          ),
          validator: (value) {
            final emailRegex = RegExp(
              r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$',
            );
            if (value == null || value.isEmpty) {
              return 'Please enter an email address';
            }
            if (!emailRegex.hasMatch(value)) {
              return 'Please enter a valid email address';
            }
            return null;
          },
        );
      },
    );
  }

  Widget _buildNextButton() {
    return SizedBox(
      width: double.infinity,
      child: GetBuilder<EmailVerifyController>(
        builder: (emailVerifyController) {
          return ElevatedButton(
            onPressed: () {
              if (!_formKey.currentState!.validate()) {
                return;
              }
              emailVerifyController.goToNextScreen(
                emailVerifyController.emailCtrl.text,
              );
              emailVerifyController.emailCtrl.clear();
            },
            child: const Text('Next'),
          );
        },
      ),
    );
  }
}
