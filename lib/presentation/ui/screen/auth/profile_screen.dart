import 'package:craftyBay/presentation/ui/widgets/header_description_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../state_holders/profile_update_controller.dart';
import '../../widgets/Resubale_text_filed.dart';

class ProfileScreen extends StatefulWidget {
  final String email;
  final String otp;

  const ProfileScreen({Key? key, required this.email, required this.otp})
      : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final ProfileUpdateController profileUpdateController =
      Get.find<ProfileUpdateController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  const HeaderDescriptionWidget(
                    title: 'Complete Profile',
                    description: "Get started with us with your details",
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Column(
                    children: [
                      ReusableTextFormFiled(
                        controller: profileUpdateController.firstNameCtrl,
                        hintText: 'First Name',
                        validator: (value) {
                          return '';
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      ReusableTextFormFiled(
                        controller: profileUpdateController.lastNameCtrl,
                        hintText: 'Last Name',
                        validator: (value) {
                          return '';
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      ReusableTextFormFiled(
                        controller: profileUpdateController.mobileCtrl,
                        hintText: 'Mobile',
                        validator: (value) {
                          return '';
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      ReusableTextFormFiled(
                        controller: profileUpdateController.cityNameCtrl,
                        hintText: 'City',
                        validator: (value) {
                          return '';
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      ReusableTextFormFiled(
                        controller: profileUpdateController.shoppingCtrl,
                        hintText: 'Shopping Address',
                        minLines: 5,
                        validator: (value) {
                          return '';
                        },
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  _buildNextButton(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildNextButton() {
    return SizedBox(
      width: double.infinity,
      child: GetBuilder<ProfileUpdateController>(
        builder: (profileUpdateController) {
          return ElevatedButton(
            onPressed: () {
              if (!_formKey.currentState!.validate()) {
                return;
              }
              profileUpdateController.firstNameCtrl.clear();
              profileUpdateController.lastNameCtrl.clear();
              profileUpdateController.mobileCtrl.clear();
              profileUpdateController.cityNameCtrl.clear();
              profileUpdateController.shoppingCtrl.clear();
            },
            child: const Text('Complete'),
          );
        },
      ),
    );
  }



}


