import 'package:craftyBay/presentation/ui/widgets/product/product_card.dart';
import 'package:flutter/material.dart';

class WishListScreen extends StatelessWidget {
  WishListScreen({Key? key}) : super(key: key);

  final List<Map<String, dynamic>> cardProductData = [
    {
      'productImgUrl': 'assets/images/product_img/main_img.png',
      'cardProductTitle': 'Product 1 New Year Special Show 30',
      'price': 50.0,
      'rating': 4.5,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_1.png',
      'cardProductTitle': 'Product 2 New Year Special Show 30',
      'price': 30.0,
      'rating': 3.8,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_2.png',
      'cardProductTitle': 'Product 3 New Year Special Show 30',
      'price': 20.0,
      'rating': null,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_3.png',
      'cardProductTitle': 'Product 4 New Year Special Show 30',
      'price': 65.0,
      'rating': 4.2,
    },
    {
      'productImgUrl': 'assets/images/product_img/main_img.png',
      'cardProductTitle': 'Product 5 New Year Special Show 30',
      'price': 40.0,
      'rating': 4.0,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_1.png',
      'cardProductTitle': 'Product 6 New Year Special Show 30',
      'price': 55.0,
      'rating': 4.8,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_2.png',
      'cardProductTitle': 'Product 7 New Year Special Show 30',
      'price': 25.0,
      'rating': 3.5,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_3.png',
      'cardProductTitle': 'Product 8 New Year Special Show 30',
      'price': 75.0,
      'rating': 4.7,
    },
    {
      'productImgUrl': 'assets/images/product_img/main_img.png',
      'cardProductTitle': 'Product 9 New Year Special Show 30',
      'price': 60.0,
      'rating': 4.3,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_1.png',
      'cardProductTitle': 'Product 10 New Year Special Show 30',
      'price': 45.0,
      'rating': 3.9,
    },
  ];

  int _calculateCrossAxisCount(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    Orientation orientation = MediaQuery.of(context).orientation;

    // You can adjust these values as needed
    if (orientation == Orientation.portrait) {
      if (screenWidth > 600) {
        return 4;
      } else {
        return 3;
      }
    } else {
      if (screenWidth > 900) {
        return 5;
      } else {
        return 4;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Colors.black,
          onPressed: () {
            // Get.back();
          },
        ),
        elevation: 0,
        title: const Text(
          'Categories',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: GridView.builder(
              itemCount: cardProductData.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: _calculateCrossAxisCount(context),
                mainAxisSpacing: 8,
                crossAxisSpacing: 8,
              ),
              itemBuilder: (context, index) {
                final productData = cardProductData[index];
                final productImgUrl = productData['productImgUrl'] as String;
                final cardProductTitle =
                    productData['cardProductTitle'] as String;
                final price = productData['price'] as double;
                final rating = productData['rating'] as double?;
                return FittedBox(
                  fit: BoxFit.fitHeight,
                  child: ProductCard(
                    productImgUrl: productImgUrl,
                    cardProductTitle: cardProductTitle,
                    price: price,
                    rating: rating,
                  ),
                );
              }),
        ),
      ),
    );
  }
}
