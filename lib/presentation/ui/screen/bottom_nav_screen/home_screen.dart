import 'package:craftyBay/presentation/ui/screen/bottom_nav_screen/category_screen.dart';
import 'package:craftyBay/presentation/ui/screen/product_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../widgets/Home/above_header_widget.dart';
import '../../widgets/Home/home_carousel_widget.dart';
import '../../widgets/Home/home_slider_card_widget.dart';
import '../../widgets/app_bar_widget.dart';
import '../../widgets/custom_search_widget.dart';
import '../../widgets/product/category_card.dart';
import '../../widgets/product/product_card.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  int current = 0;

  List<HomeSliderCard> homeSliderData = [
    HomeSliderCard(
      imgUrl: 'assets/images/product_img/main_img.png',
      productTitle: 'Product Title 1 goes here. Look at this',
      onTap: () {
        // Navigate to the next page
      },
    ),
    HomeSliderCard(
      imgUrl: 'assets/images/product_img/produt_1.png',
      productTitle: 'Product Title 2 goes here. Look at this',
      onTap: () {
        // Navigate to the next page
      },
    ),
    HomeSliderCard(
      imgUrl: 'assets/images/product_img/produt_2.png',
      productTitle: 'Product Title 3 goes here. Look at this',
      onTap: () {
        // Navigate to the next page
      },
    ),
    HomeSliderCard(
      imgUrl: 'assets/images/product_img/produt_3.png',
      productTitle: 'Product Title 3 goes here. Look at this',
      onTap: () {
        // Navigate to the next page
      },
    ),
  ];

  List<String> cateIcon = [
    'assets/images/product_img/cat_img_1.png',
    'assets/images/product_img/cat_img_2.png',
    'assets/images/product_img/cat_img_3.png',
    'assets/images/product_img/cat_img_4.png',
    'assets/images/product_img/cat_img_1.png',
    'assets/images/product_img/cat_img_2.png',
    'assets/images/product_img/cat_img_3.png',
    'assets/images/product_img/cat_img_4.png',
  ];


  List<Map<String, dynamic>> cardProductData = [
    {
      'productImgUrl': 'assets/images/product_img/main_img.png',
      'cardProductTitle': 'Nike Shoe Ak300 Show 30',
      'price': 50.0,
      'rating': 4.5,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_1.png',
      'cardProductTitle': 'Product 2 New Year Special Show 30',
      'price': 30.0,
      'rating': 3.8,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_2.png',
      'cardProductTitle': 'Product 3 New Year Special Show 30',
      'price': 20.0,
      'rating': null,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_3.png',
      'cardProductTitle': 'Product 4 New Year Special Show 30',
      'price': 65.0,
      'rating': 4.2,
    },
    {
      'productImgUrl': 'assets/images/product_img/main_img.png',
      'cardProductTitle': 'Product 5 New Year Special Show 30',
      'price': 40.0,
      'rating': 4.0,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_1.png',
      'cardProductTitle': 'Product 6 New Year Special Show 30',
      'price': 55.0,
      'rating': 4.8,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_2.png',
      'cardProductTitle': 'Product 7 New Year Special Show 30',
      'price': 25.0,
      'rating': 3.5,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_3.png',
      'cardProductTitle': 'Product 8 New Year Special Show 30',
      'price': 75.0,
      'rating': 4.7,
    },
    {
      'productImgUrl': 'assets/images/product_img/main_img.png',
      'cardProductTitle': 'Product 9 New Year Special Show 30',
      'price': 60.0,
      'rating': 4.3,
    },
    {
      'productImgUrl': 'assets/images/product_img/produt_1.png',
      'cardProductTitle': 'Product 10 New Year Special Show 30',
      'price': 45.0,
      'rating': 3.9,
    },
  ];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const CustomSearchWidget(),
                const SizedBox(
                  height: 10,
                ),
                CarouselWithDots(
                  items: homeSliderData,
                ),
                const SizedBox(
                  height: 10,
                ),
                AboveHeaderWidget(
                  catTitle: 'All Categories',
                  onTap: () {
                    Get.to(() => CategoryScreen());
                  },
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  height: 80,
                  child: ListView.separated(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index){
                      return CategoryCard(cateIcon: cateIcon[index],);
                    },
                    separatorBuilder: (context, index){
                      return const SizedBox(width: 10,);
                    },
                    itemCount: cateIcon.length,
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                AboveHeaderWidget(
                  catTitle: 'Popular',
                  onTap: () {
                    Get.to(() => ProductListScreen());
                  },
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  height: 135,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: cardProductData.length,
                    itemBuilder: (context, index) {
                      final productData = cardProductData[index];
                      final productImgUrl = productData['productImgUrl'] as String;
                      final cardProductTitle = productData['cardProductTitle'] as String;
                      final price = productData['price'] as double;
                      final rating = productData['rating'] as double?;
                      return ProductCard(
                        productImgUrl: productImgUrl,
                        cardProductTitle: cardProductTitle,
                        price: price,
                        rating: rating,
                      );
                    },
                    separatorBuilder: (context, index) {
                      return const SizedBox(
                        width: 3,
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                AboveHeaderWidget(
                  catTitle: 'Special',
                  onTap: () {
                    Get.to(() => ProductListScreen());
                  },
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  height: 135,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: cardProductData.length,
                    itemBuilder: (context, index) {
                      final productData = cardProductData[index];
                      final productImgUrl = productData['productImgUrl'] as String;
                      final cardProductTitle = productData['cardProductTitle'] as String;
                      final price = productData['price'] as double;
                      final rating = productData['rating'] as double?;
                      return ProductCard(
                        productImgUrl: productImgUrl,
                        cardProductTitle: cardProductTitle,
                        price: price,
                        rating: rating,

                      );
                    },
                    separatorBuilder: (context, index) {
                      return const SizedBox(
                        width: 3,
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 7,
                ),
                AboveHeaderWidget(
                  catTitle: 'New',
                  onTap: () {
                    Get.to(() => ProductListScreen());
                  },
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  height: 135,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: cardProductData.length,
                    itemBuilder: (context, index) {
                      final productData = cardProductData[index];
                      final productImgUrl = productData['productImgUrl'] as String;
                      final cardProductTitle = productData['cardProductTitle'] as String;
                      final price = productData['price'] as double;
                      final rating = productData['rating'] as double?;
                      return ProductCard(
                        productImgUrl: productImgUrl,
                        cardProductTitle: cardProductTitle,
                        price: price,
                        rating: rating,
                      );
                    },
                    separatorBuilder: (context, index) {
                      return const SizedBox(
                        width: 3,
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

