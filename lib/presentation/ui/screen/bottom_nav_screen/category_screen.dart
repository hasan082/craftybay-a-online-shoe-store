import 'package:craftyBay/presentation/ui/widgets/product/category_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoryScreen extends StatelessWidget {
  CategoryScreen({Key? key}) : super(key: key);

  final List<String> cateIcon = [
    'assets/images/product_img/cat_img_1.png',
    'assets/images/product_img/cat_img_2.png',
    'assets/images/product_img/cat_img_3.png',
    'assets/images/product_img/cat_img_4.png',
    'assets/images/product_img/cat_img_1.png',
    'assets/images/product_img/cat_img_2.png',
    'assets/images/product_img/cat_img_3.png',
    'assets/images/product_img/cat_img_4.png',
    'assets/images/product_img/cat_img_1.png',
    'assets/images/product_img/cat_img_2.png',
    'assets/images/product_img/cat_img_3.png',
    'assets/images/product_img/cat_img_4.png',
  ];

  int _calculateCrossAxisCount(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    Orientation orientation = MediaQuery.of(context).orientation;

    // You can adjust these values as needed
    if (orientation == Orientation.portrait) {
      if (screenWidth > 600) {
        return 6;
      } else {
        return 4;
      }
    } else {
      if (screenWidth > 900) {
        return 7;
      } else {
        return 5;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Colors.black,
          onPressed: (){
            Get.back();
          },
        ),
        elevation: 0,
        title: const Text(
          'Categories',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: GridView.builder(
            itemCount: cateIcon.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: _calculateCrossAxisCount(context),
                mainAxisSpacing: 8,
                crossAxisSpacing: 8,
              ),
              itemBuilder: (context, index) {
                return FittedBox(child: CategoryCard(cateIcon: cateIcon[index],));
              }),
        ),
      ),
    );
  }
}
