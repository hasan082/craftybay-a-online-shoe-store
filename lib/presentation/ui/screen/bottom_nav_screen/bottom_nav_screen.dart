import 'package:craftyBay/presentation/ui/screen/bottom_nav_screen/wishlist_screen.dart';
import 'package:craftyBay/presentation/ui/widgets/custom_bottom_navigation_bar_widget.dart';
import 'package:flutter/material.dart';
import 'cart_screen.dart';
import 'category_screen.dart';
import 'home_screen.dart';

class BottomNavScreen extends StatefulWidget {
  const BottomNavScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavScreen> createState() => _BottomNavScreenState();
}

class _BottomNavScreenState extends State<BottomNavScreen> {
  int _selectedIndex = 0;
  final PageController _pageController = PageController();

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  static final List<PageData> _pageData = [
    PageData(
      page: const HomePage(),
      icon: Icons.home,
      title: 'Home',
    ),
    PageData(
      page: CategoryScreen(),
      icon: Icons.category,
      title: 'Categories',
    ),
    PageData(
        page: const CartScreen(),
        icon: Icons.shopping_cart_checkout,
        title: 'Cart'),
    PageData(
      page: WishListScreen(),
      icon: Icons.wallet_giftcard_rounded,
      title: 'Wish',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: _pageData.map((pageData) => pageData.page).toList(),
        onPageChanged: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
      bottomNavigationBar: CustomBottomNavigationBar(
        selectedIndex: _selectedIndex,
        pageController: _pageController,
        onTap: (index) {
          _selectedIndex = index;
          _pageController.animateToPage(
            index,
            duration: const Duration(milliseconds: 20),
            curve: Curves.ease,
          );
          setState(() {});
        },
        pageData: _pageData,
      ),
    );
  }
}
