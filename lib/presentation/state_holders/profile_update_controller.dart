import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';


class ProfileUpdateController extends GetxController {

  final TextEditingController firstNameCtrl = TextEditingController();
  final TextEditingController lastNameCtrl = TextEditingController();
  final TextEditingController mobileCtrl = TextEditingController();
  final TextEditingController cityNameCtrl = TextEditingController();
  final TextEditingController shoppingCtrl = TextEditingController();


  void goToNextScreen(String email) {

  }

  @override
  void dispose() {
    super.dispose();
    firstNameCtrl.dispose();
    lastNameCtrl.dispose();
    mobileCtrl.dispose();
    cityNameCtrl.dispose();
    shoppingCtrl.dispose();
  }


}