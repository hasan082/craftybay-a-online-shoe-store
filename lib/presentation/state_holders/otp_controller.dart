import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ui/screen/auth/profile_screen.dart';


class OtpVerifyController extends GetxController {
  final TextEditingController otpCtrl = TextEditingController();
  final int initialCountdown = 120;
  late Timer countdownTimer; // Renamed _timer to countdownTimer
  RxInt countDown = 120.obs;
  RxBool isTimerRunning = false.obs;
  RxBool isFinished = false.obs;

  void startCountDownTimer() {
    countDown.value = initialCountdown;
    isTimerRunning.value = true;
    isFinished.value = false;
    const oneSec = Duration(seconds: 1);
    countdownTimer = Timer.periodic(
      oneSec,
          (Timer timer) {
        if (countDown.value == 0) {
          timer.cancel();
          isTimerRunning.value = false;
          isFinished.value = true;
        } else {
          countDown.value--;
        }
      },
    );
  }

  void resetCountdownTimer() {
    if (!isTimerRunning.value) {
      startCountDownTimer();
    }
  }

  void gotoNextScreenWithOtpAndEmail(String email, String otp) {
    Get.offAll(() => ProfileScreen(email: email, otp: otp,));
  }

  @override
  void dispose() {
    super.dispose();
    countdownTimer.cancel();
    otpCtrl.dispose();
  }
}
