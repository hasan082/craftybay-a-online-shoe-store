import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/route_manager.dart';
import '../ui/screen/auth/otp_verify_screen.dart';

class EmailVerifyController extends GetxController {


  final TextEditingController emailCtrl = TextEditingController();

  void goToNextScreen(String email) {
    Get.to(() => OtpVerifyScreen(email: email));
  }

  @override
  void dispose() {
    super.dispose();
    emailCtrl.dispose();
  }


}